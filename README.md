# Build GraalVM EE on Gitlab CI/CD using Gitlab Runner

This is a simple demo showing how to use the
[Gitlab runner](https://docs.gitlab.com/runner/) to create a native image
application using the
[Oracle GraalVM Enterprise](https://www.oracle.com/java/graalvm/).

A Gitlab runner is similar to a
[Jenkins agent](https://www.jenkins.io/doc/book/using/using-agents/). The Gitlab
pipeline will delegate tasks to the runner, such as build our application.

An OCI instance is created and setup to build this application. A different
linux instance can be used. The **linux** instance needs to be **64 bit** and
running on the **amd64** architecture. Please note that some native image
features are only available to the linux amd64 architecture.

## Setup

Some commands are executed on the host machine (a Mac laptop in my case) while
the rest of the commands are executed on the OCI instance. To make a distinction
between the two, a different terminal symbol is used.

- Commands executed on the host machine (`>`)

  ```shell
  > exec on host machine
  ```

- Commands executed on the OCI instance where the Gitlab runner will be
  installed and running (`$`)

  ```shell
  $ exec on OCI instance
  ```

The setup is divided into four sections

- [Create OCI Linux amd64 instance](#create-oci-linux-amd64-instance)
- [Install tools required to build the native image (_on the OCI Linux amd64 instance_)](#install-tools-required-to-build-the-native-image-_on-the-oci-linux-amd64-instance_)
- [Install GraalVM (_on the OCI Linux amd64 instance_)](#install-graalvm-_on-the-oci-linux-amd64-instance_)
- [Install Gitlab Runner (_on the OCI Linux amd64 instance_)](#install-gitlab-runner-_on-the-oci-linux-amd64-instance_)

It is recommended to run these in the specified order

### Create OCI Linux amd64 instance

1. Create a publicly accessible Oracle Linux 9 (Intel or AMD) with 2 CPU and
   16 GB+ RAM ([OCI compute](https://cloud.oracle.com/compute/instances/)).
   Set up the SSH keys and wait for the instance to be created. This takes a
   couple of minutes.

   When the OCI instance is ready, copy the public IP address
   (`150.136.150.26` in this example).

2. Configure the ssh on the host machine so that you can log into the OCI
   instance.

   ```shell
   > vi ~/.ssh/config
   ```

   Add a new entry or update an existing one.

   ```
   Host 150.136.150.26
     Hostname 150.136.150.26
     user opc
     port 22
     identityfile ~/.ssh/ssh-key-2077-04-27.key
   ```

   Otherwise, use the `-i` to specify the identity file.

3. Log into the OCI instance (from the host machine)

   ```shell
   > ssh opc@150.136.150.26
   ```

   Or use the `-i` to specify the identity file

   ```shell
   > ssh -i ~/.ssh/ssh-key-2077-04-27.key opc@150.136.150.26
   ```

   Change the IP address `150.136.150.26` to the IP address of the OCI instance.

### Install tools required to build the native image (_on the OCI Linux amd64 instance_)

1. Install GCC and other tools on the OCI instance that are required by Gitlab
   runner to build the native image

   ```shell
   $ sudo dnf config-manager --set-enabled ol9_codeready_builder
   $ sudo dnf install git-all gcc glibc-devel zlib-devel libstdc++-static make -y
   ```

   This is a generic installation, suitable for different native image builds,
   such as
   [statically linked image](https://docs.oracle.com/en/graalvm/enterprise/21/docs/reference-manual/native-image/StaticImages/).

### Install GraalVM (_on the OCI Linux amd64 instance_)

1. Create download directory

   ```shell
   $ mkdir --parents ~/downloads
   ```

   The external binaries are downloaded into the `~/downloads` temporary
   directory. This directory will be removed at the end of the setup.

2. Install Oracle GraalVM Enterprise and the `native-image` component

   Go to the Oracle GraalVM Enterprise Edition 22
   [download page](https://www.oracle.com/downloads/graalvm-downloads.html),
   using **a web browser** from the host machine, and set the following.

   | Field        | Value     |
   | ------------ | --------- |
   | Java Version | **17**    |
   | OS           | **Linux** |
   | Architecture | **x86**   |

   Download both the **Oracle GraalVM Enterprise Edition JDK** and the
   **Oracle GraalVM Enterprise Edition Native Image** after agreeing to the
   terms and conditions.

   Copy the two downloaded files from the host machine to the OCI instance.

   ```shell
   > scp \
     ~/Downloads/graalvm-ee-java17-linux-amd64-22.3.1.tar.gz \
     opc@150.136.150.26:/home/opc/downloads/
   > scp \
     ~/Downloads/native-image-installable-svm-svmee-java17-linux-amd64-22.3.1.jar \
     opc@150.136.150.26:/home/opc/downloads/
   ```

   Change the IP address `150.136.150.26` to the IP address of the OCI instance.

   Install Oracle GraalVM Enterprise on the OCI instance

   ```shell
   $ cd ~/downloads
   $ tar xvfz graalvm-ee-java17-linux-amd64-22.3.1.tar.gz
   $ rm graalvm-ee-java17-linux-amd64-22.3.1.tar.gz
   $ sudo mv graalvm-ee-java17-22.3.1 /opt/graal
   ```

   Set the environment variables for everyone on the OCI instance

   ```shell
   $ sudo touch '/etc/profile.d/graal.sh'
   $ sudo vi '/etc/profile.d/graal.sh'
   ```

   Add the following environment variables

   ```
   export JAVA_HOME="/opt/graal"
   export PATH="${PATH}:${JAVA_HOME}/bin"
   ```

   Set the environment variables for the current session

   ```shell
   $ export JAVA_HOME="/opt/graal"
   $ export PATH="${PATH}:${JAVA_HOME}/bin"
   ```

   Install the GraalVM `native-image` component

   ```shell
   $ gu install --local-file ~/downloads/native-image-installable-svm-svmee-java17-linux-amd64-22.3.1.jar
   $ rm ~/downloads/native-image-installable-svm-svmee-java17-linux-amd64-22.3.1.jar
   ```

   Verify that both the `graalvm` and the `native-image` components are
   installed

   ```shell
   $ gu list
   ```

   This will list all installed components and their respective versions

   ```
   ComponentId              Version             Component name                Stability                     Origin
   ---------------------------------------------------------------------------------------------------------------------------------
   graalvm                  22.3.1              GraalVM Core                  Supported
   native-image             22.3.1              Native Image                  Early adopter
   ```

3. Extract [musl GCC toolchains](https://musl.cc/)

   Musl ia a small and reliable pre-built GCC toolchains for many
   architectures, required by the `native-image` tool. We need to install the
   `10.x` version as the `11.x` version has some issues
   ([reference](https://docs.oracle.com/en/graalvm/enterprise/21/docs/reference-manual/native-image/StaticImages/)).

   ```shell
   $ cd ~/downloads
   $ curl http://more.musl.cc/10/x86_64-linux-musl/x86_64-linux-musl-native.tgz \
     --output ~/downloads/x86_64-linux-musl-native.tgz
   $ tar xfz x86_64-linux-musl-native.tgz
   $ rm x86_64-linux-musl-native.tgz
   $ sudo mv x86_64-linux-musl-native /opt/musl
   ```

   Set the environment variables for everyone on the OCI instance. Please note
   that the Gitlab runner will be running as a different user, and these
   settings need to works for the Gitlab runner user.

   ```shell
   $ sudo vi '/etc/profile.d/graal.sh'
   ```

   Add the following environment variables

   ```
   export TOOLCHAIN_DIR="/opt/musl"
   export CC="${TOOLCHAIN_DIR}/bin/gcc"
   export PATH="${PATH}:${TOOLCHAIN_DIR}/bin"
   ```

   Set the environment variables for the current session

   ```shell
   $ export TOOLCHAIN_DIR="/opt/musl"
   $ export CC="${TOOLCHAIN_DIR}/bin/gcc"
   $ export PATH="${PATH}:${TOOLCHAIN_DIR}/bin"
   ```

4. Install [zlib](https://zlib.net)

   A compression library required to build the musl GCC toolchains

   ```shell
   $ cd ~/downloads
   $ curl https://zlib.net/zlib-1.2.13.tar.gz \
     --output ~/downloads/zlib-1.2.13.tar.gz
   $ tar xfz zlib-1.2.13.tar.gz
   $ rm zlib-1.2.13.tar.gz
   $ mv zlib-1.2.13 zlib
   ```

5. Compile and install zlib into the toolchain

   ```shell
   $ cd ~/downloads/zlib
   $ ./configure "--prefix=${TOOLCHAIN_DIR}" --static
   $ sudo make
   $ sudo make install
   $ sudo mkdir -p "$JAVA_HOME/lib/static/linux-amd64/musl"
   $ sudo cp libz.a "$JAVA_HOME/lib/static/linux-amd64/musl"
   ```

   (_Optional_) Note that once the files are copied, this directory is not
   needed and can be safely deleted.

   ```shell
   $ cd ~/downloads
   $ rm -rf zlib
   ```

6. (_Optional_) Delete the `~/downloads` directory

   The `~/downloads` directory should be empty by now and thus can be deleted.

   ```shell
   $ cd ~
   $ rm -rf ~/downloads
   ```

### Install Gitlab Runner (_on the OCI Linux amd64 instance_)

Based on the
[Gitlab documentation](https://docs.gitlab.com/runner/install/linux-manually.html).
When using the `sudo`, I had to provide the full path to the `gitlab-runner`.

1. Download the binaries

   ```shell
   $ sudo curl -L \
     --output /usr/local/bin/gitlab-runner \
     "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
   ```

2. Give it permissions to execute

   ```shell
   $ sudo chmod +x /usr/local/bin/gitlab-runner
   ```

3. Create a GitLab CI user:

   ```shell
   $ sudo useradd \
     --comment 'GitLab Runner' \
     --create-home gitlab-runner \
     --shell /bin/bash
   ```

4. Install and run the GitLab runner as service

   ```shell
   $ sudo /usr/local/bin/gitlab-runner install \
     --user=gitlab-runner \
     --working-directory=/home/gitlab-runner
    ```

   ```
   Runtime platform                                    arch=amd64 os=linux pid=5855 revision=d540b510 version=15.9.1
   ```

   Start the runner

   ```shell
   $ sudo /usr/local/bin/gitlab-runner start
   ```

   ```
   Runtime platform                                    arch=amd64 os=linux pid=5934 revision=d540b510 version=15.9.1
   ```

5. Verify that the runner is running

   ```shell
   $ sudo /usr/local/bin/gitlab-runner status
   ```

   ```
   Runtime platform                                    arch=amd64 os=linux pid=5954 revision=d540b510 version=15.9.1
   gitlab-runner: Service is running
   ```

6. (_Optional_) View the service configuration

   ```shell
   $ sudo cat /etc/systemd/system/gitlab-runner.service
   ```

7. Register the Gitlab runner with the Gitlab project

   Get the URL and token from the
   [project](https://gitlab.com/albertattard/build-oracle-graalvm-enterprise-on-gitlab-cicd-using-gitlab-runner/-/settings/ci_cd#js-runners-settings).

   ```shell
   $ sudo /usr/local/bin/gitlab-runner register
   ```

   The registration process is interactive and will ask you to provide some
   input.

   | Input                                                                                                                                       | Value                                                                                                                                                       |
   | ------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
   | Enter the GitLab instance URL (for example, https://gitlab.com/):                                                                           | `https://gitlab.com/`                                                                                                                                       |
   | Enter the registration token:                                                                                                               | Get it from [here](https://gitlab.com/albertattard/build-oracle-graalvm-enterprise-on-gitlab-cicd-using-gitlab-runner/-/settings/ci_cd#js-runners-settings) |
   | Enter a description for the runner:                                                                                                         | `graalvm-ee-gitlab-runner`                                                                                                                                  |
   | Enter tags for the runner (comma-separated):                                                                                                | `graalvmee17,java17`                                                                                                                                        |
   | Enter optional maintenance note for the runner:                                                                                             | You can leave this blank                                                                                                                                    |
   | Enter an executor: docker+machine, kubernetes, custom, docker-ssh, parallels, shell, instance, docker, ssh, virtualbox, docker-ssh+machine: | `shell`                                                                                                                                                     |

   While different values can be used, please note that this pipeline uses the
   `graalvmee17` tag as shown below. Please make sure that `graalvmee17` is one
   of the tags, otherwise the Gitlab runner will not run.

   File: [.gitlab-ci.yml](./.gitlab-ci.yml)
   ```yaml
   default:
     tags:
       - graalvmee17
   ```

   In this example the _shell_ is selected as the executor as the native image
   will be built directly on the OCI instance. Other executors are also valid
   and may be more suitable for other scenarios.

   (_Optional_) View the created configuration

   ```shell
   $ sudo cat /etc/gitlab-runner/config.toml
   ```

   ```toml
   concurrent = 1
   check_interval = 0
   shutdown_timeout = 0

   [session_server]
     session_timeout = 1800

   [[runners]]
     name = "graalvm-ee-gitlab-runner"
     url = "https://gitlab.com/"
     id = 21830170
     token = "ro6gWnaQR2wmmqPr24sG"
     token_obtained_at = 2077-04-27T12:34:56Z
     token_expires_at = 0001-01-01T00:00:00Z
     executor = "shell"
     [runners.cache]
       MaxUploadedArchiveSize = 0
   ```

8. Verify the runner

   ```shell
   $ sudo /usr/local/bin/gitlab-runner verify
   ```

   ```
   Runtime platform                                    arch=amd64 os=linux pid=36965 revision=d540b510 version=15.9.1
   Running in system-mode.

   Verifying runner... is alive                        runner=ro6gWnaQ
   ```

9. Verify that the project runner is added

   From the menu, go to
   [_Settings_ > _CI/CD_](https://gitlab.com/albertattard/build-oracle-graalvm-enterprise-on-gitlab-cicd-using-gitlab-runner/-/settings/ci_cd)

   ![Project runner](./assets/images/Graalvm-EE-Gitlab-Runner.png)

10. Disable the shared runners

   From the menu, go to
   [_Settings_ > _CI/CD_](https://gitlab.com/albertattard/build-oracle-graalvm-enterprise-on-gitlab-cicd-using-gitlab-runner/-/settings/ci_cd)

   ![Shared runners](./assets/images/Shared-runners.png)

11. Push a change (trigger the pipeline manually)

   From the menu, go to
   [_CI/CD_ > _pipelines_](https://gitlab.com/albertattard/build-oracle-graalvm-enterprise-on-gitlab-cicd-using-gitlab-runner/-/pipelines)
